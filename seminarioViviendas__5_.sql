-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 24-10-2021 a las 21:41:09
-- Versión del servidor: 8.0.26-0ubuntu0.20.04.2
-- Versión de PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `seminarioViviendas`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuenta`
--

CREATE TABLE `cuenta` (
  `usuario` varchar(20) NOT NULL,
  `contrasena` varchar(20) NOT NULL,
  `email` varchar(20) NOT NULL,
  `activo` tinyint(1) NOT NULL,
  `rol` int NOT NULL,
  `id_cuenta` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Direccion`
--

CREATE TABLE `Direccion` (
  `calle` varchar(15) NOT NULL,
  `numero` int NOT NULL,
  `barrio` varchar(15) NOT NULL,
  `id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `Direccion`
--

INSERT INTO `Direccion` (`calle`, `numero`, `barrio`, `id`) VALUES
('12312', 123123, '12312', 7),
('12312', 123123, '12312', 8),
('12312', 1231, '123', 9),
('432', 12421, '4124', 10),
('1231', 213123, '213213', 11),
('13', 3131, '13', 12),
('432', 165, '165', 13);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Pedido_retiro`
--

CREATE TABLE `Pedido_retiro` (
  `id_pedido` int NOT NULL,
  `fecha_del_pedido` date NOT NULL,
  `observacion` varchar(30) NOT NULL,
  `numero_vivienda` int NOT NULL,
  `vehiculo_pesado` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `Pedido_retiro`
--

INSERT INTO `Pedido_retiro` (`id_pedido`, `fecha_del_pedido`, `observacion`, `numero_vivienda`, `vehiculo_pesado`) VALUES
(39, '2021-10-24', '2131', 4, 0),
(40, '2021-10-24', '2131', 4, 0),
(41, '2021-10-24', 'sdas', 3, 0),
(42, '2021-10-24', 'holaa', 4, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Propietario`
--

CREATE TABLE `Propietario` (
  `nombre` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `apellido` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `dni` varchar(15) NOT NULL,
  `id_cuenta` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `Propietario`
--

INSERT INTO `Propietario` (`nombre`, `apellido`, `dni`, `id_cuenta`) VALUES
('123', '123', '123', 0),
('213213', '123123', '123123', 0),
('1241', '214', '14', 0),
('321', '213', '213', 0),
('313', '13', '3', 0),
('56', '465', '456', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Residuo`
--

CREATE TABLE `Residuo` (
  `id` int NOT NULL,
  `id_pedido` int NOT NULL,
  `id_tipo_residuo` int NOT NULL,
  `peso` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `Residuo`
--

INSERT INTO `Residuo` (`id`, `id_pedido`, `id_tipo_residuo`, `peso`) VALUES
(6, 39, 1, 22),
(7, 40, 1, 22),
(8, 40, 2, 22),
(9, 41, 1, 50),
(10, 41, 3, 50),
(11, 42, 2, 10);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `codigo` int NOT NULL,
  `nombre` varchar(20) NOT NULL,
  `activo` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`codigo`, `nombre`, `activo`) VALUES
(1, 'kko', 1),
(2, '', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Tipo_residuo`
--

CREATE TABLE `Tipo_residuo` (
  `id` int NOT NULL,
  `tipo_residuo` varchar(30) NOT NULL,
  `puntos_kilo` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `Tipo_residuo`
--

INSERT INTO `Tipo_residuo` (`id`, `tipo_residuo`, `puntos_kilo`) VALUES
(1, 'carton', 0),
(2, 'vidrio', 0),
(3, 'plastico', 0),
(4, 'escombro', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Vehiculo`
--

CREATE TABLE `Vehiculo` (
  `patente` varchar(15) NOT NULL,
  `marca` varchar(20) NOT NULL,
  `modelo` varchar(20) NOT NULL,
  `tipo_carga` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Vivienda`
--

CREATE TABLE `Vivienda` (
  `numero_vivienda` int NOT NULL,
  `dniPropietario` varchar(15) NOT NULL,
  `idDireccion` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `Vivienda`
--

INSERT INTO `Vivienda` (`numero_vivienda`, `dniPropietario`, `idDireccion`) VALUES
(1, '14', 10),
(2, '213', 11),
(3, '3', 12),
(4, '456', 13);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cuenta`
--
ALTER TABLE `cuenta`
  ADD PRIMARY KEY (`id_cuenta`),
  ADD KEY `fk_idrol` (`rol`);

--
-- Indices de la tabla `Direccion`
--
ALTER TABLE `Direccion`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `Pedido_retiro`
--
ALTER TABLE `Pedido_retiro`
  ADD PRIMARY KEY (`id_pedido`);

--
-- Indices de la tabla `Propietario`
--
ALTER TABLE `Propietario`
  ADD PRIMARY KEY (`dni`);

--
-- Indices de la tabla `Residuo`
--
ALTER TABLE `Residuo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_residuo` (`id_pedido`,`id_tipo_residuo`),
  ADD KEY `id_tipo_residuo` (`id_tipo_residuo`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`codigo`);

--
-- Indices de la tabla `Tipo_residuo`
--
ALTER TABLE `Tipo_residuo`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `Vehiculo`
--
ALTER TABLE `Vehiculo`
  ADD PRIMARY KEY (`patente`);

--
-- Indices de la tabla `Vivienda`
--
ALTER TABLE `Vivienda`
  ADD PRIMARY KEY (`numero_vivienda`),
  ADD KEY `fk_direccion` (`idDireccion`),
  ADD KEY `fk_dni` (`dniPropietario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cuenta`
--
ALTER TABLE `cuenta`
  MODIFY `id_cuenta` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `Direccion`
--
ALTER TABLE `Direccion`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `Pedido_retiro`
--
ALTER TABLE `Pedido_retiro`
  MODIFY `id_pedido` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT de la tabla `Residuo`
--
ALTER TABLE `Residuo`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `codigo` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `Tipo_residuo`
--
ALTER TABLE `Tipo_residuo`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `Vivienda`
--
ALTER TABLE `Vivienda`
  MODIFY `numero_vivienda` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `cuenta`
--
ALTER TABLE `cuenta`
  ADD CONSTRAINT `fk_idrol` FOREIGN KEY (`rol`) REFERENCES `roles` (`codigo`);

--
-- Filtros para la tabla `Residuo`
--
ALTER TABLE `Residuo`
  ADD CONSTRAINT `Residuo_ibfk_1` FOREIGN KEY (`id_tipo_residuo`) REFERENCES `Tipo_residuo` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `Residuo_ibfk_2` FOREIGN KEY (`id_pedido`) REFERENCES `Pedido_retiro` (`id_pedido`) ON DELETE CASCADE;

--
-- Filtros para la tabla `Vivienda`
--
ALTER TABLE `Vivienda`
  ADD CONSTRAINT `fk_direccion` FOREIGN KEY (`idDireccion`) REFERENCES `Direccion` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_dni` FOREIGN KEY (`dniPropietario`) REFERENCES `Propietario` (`dni`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
